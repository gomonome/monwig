package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/gomonome/monome"
	"gitlab.com/gomonome/monwig"
	"gitlab.com/goosc/bitwig"
	"gitlab.com/metakeule/config"
)

var (
	cfg    = config.MustNew("monwig", "0.2.0", "control bitwig studio with monome")
	inAddr = cfg.NewString("in", "address where bitwig sends to and monome should receive from.",
		config.Default("127.0.0.1:9000"))
	outAddr = cfg.NewString("out", "address where bitwig receives from and monome should sends to.",
		config.Default("127.0.0.1:8000"))
)

func run() error {
	err := cfg.Run()
	if err != nil {
		return err
	}

	var sigchan = make(chan os.Signal, 10)

	m := monwig.New()
	err = m.Connect(bitwig.New(m, bitwig.ListenAddress(inAddr.Get()), bitwig.WriteAddress(outAddr.Get())), func(err error) {
		sigchan <- os.Interrupt
	})

	if err != nil {
		return err
	}

	fmt.Fprintf(os.Stdout, "listening on %v and writing to %v\n", inAddr.Get(), outAddr.Get())

	// listen for ctrl+c
	go signal.Notify(sigchan, os.Interrupt)

	// interrupt has happend
	<-sigchan

	fmt.Fprint(os.Stdout, "\ninterrupted, cleaning up...")
	monome.SwitchAll(m, false)
	m.Close()
	fmt.Fprintln(os.Stdout, "done")
	return nil
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		os.Exit(1)
	}
	os.Exit(0)
}
