module gitlab.com/gomonome/monwig

require (
	gitlab.com/gomonome/dawcontrol v0.6.0
	gitlab.com/gomonome/monome v0.4.0
	gitlab.com/goosc/bitwig v0.0.8
	gitlab.com/goosc/osc v0.2.0
	gitlab.com/metakeule/config v1.13.0
)
