package monwig

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/gomonome/dawcontrol"
	"gitlab.com/goosc/bitwig"
	"gitlab.com/goosc/osc"
)

func New() dawcontrol.Connection {
	return dawcontrol.New("monwig", &monwig{})
}

type monwig struct {
	layout           dawcontrol.Layout
	playing          bool
	writeAutomation  bool
	selectedClip     [2]uint8
	showFilledMode   bool
	fillOrDeleteMode bool
	sync.RWMutex
}

func (a *monwig) SetLayout(h dawcontrol.Layout) {
	a.layout = h
}

func (a *monwig) Matches(path osc.Path) bool {
	if path == "/time/str" || path.HasSuffix("vu") || path == "/beat/str" {
		return false
	}
	return true
}

func (a *monwig) HasSelectedClip() bool {
	a.RLock()
	has := a.hasSelectedClip()
	a.RUnlock()
	return has
}

func (a *monwig) InFillOrDeleteMode() bool {
	a.RLock()
	is := a.fillOrDeleteMode
	a.RUnlock()
	return is
}

func (a *monwig) hasSelectedClip() bool {
	return a.selectedClip[0] > 0
}

func (a *monwig) SelectedClip() (track, clip uint8) {
	if !a.HasSelectedClip() {
		panic("no selected clip")
	}
	a.RLock()
	track = a.selectedClip[0] - 1
	clip = a.selectedClip[1] - 1
	a.RUnlock()
	return
}

func (a *monwig) InShowFilledMode() bool {
	a.RLock()
	is := a.showFilledMode
	a.RUnlock()
	return is
}

func (a *monwig) IsPlaying() bool {
	a.RLock()
	is := a.playing
	a.RUnlock()
	return is
}

func (a *monwig) doFillOrDelete(track, clip uint8) {
	if !a.layout.IsFilledClip(track, clip) {
		a.layout.FillClip(track, clip)
		return
	}

	a.layout.UnfillClip(track, clip)
	if a.layout.ActiveTrackClip(track) == clip+1 {
		a.layout.SetActiveTrackClip(track, 0)
	}

	//	fmt.Printf("deleting: track %d, clip %d\n", track+1, clip+1)
	//	a.playtime.SelectClip(track+1, clip+1)
	//a.playtime.DeleteClip()
}

func (a *monwig) ActionReleased(btn dawcontrol.ActionButton) {
	switch btn {
	case dawcontrol.DELETE_BUTTON:
		a.Lock()
		a.showFilledMode = false
		a.fillOrDeleteMode = false
		a.Unlock()
		time.Sleep(time.Millisecond * 140)
		a.layout.BlankAllClips()
	case dawcontrol.TRIGGER_BUTTON:
		var selectedClip [2]uint8
		hasSelectedClip := a.hasSelectedClip()
		a.Lock()
		a.showFilledMode = false
		selectedClip = a.selectedClip
		a.Unlock()
		if hasSelectedClip {
			a.layout.SetActiveTrackClip(selectedClip[0]-1, selectedClip[1])
			a.layout.FillClip(selectedClip[0]-1, selectedClip[1]-1)
			a.layout.SwitchClip(selectedClip[0]-1, selectedClip[1]-1, true)
			if selectedClip[1] > 8 {
				//		/scene/bank/{+,-}
				bitwig.SceneBankNext.WriteTo(a.layout)
			} else {
				bitwig.SceneBankPrevious.WriteTo(a.layout)
			}
			time.Sleep(time.Millisecond * 5)
			bitwig.TrackClipLaunch.Set(selectedClip[0], a.clipNumber(selectedClip[1]-1)).WriteTo(a.layout)
		}
		time.Sleep(time.Millisecond * 140)
		a.layout.BlankAllClips()
	case dawcontrol.PLAY_BUTTON, dawcontrol.AUTOMATION_BUTTON, dawcontrol.METRONOME_BUTTON, dawcontrol.OVERDUB_BUTTON:
		return
	}
	a.layout.SwitchActionButton(btn, false)
}

/*
"box" : 				{
					"bgcolor" : [ 0.678431, 0.819608, 0.819608, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0.79,
					"bgfillcolor_color" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
					"bgfillcolor_color1" : [ 0.678431, 0.819608, 0.819608, 1.0 ],
					"bgfillcolor_color2" : [ 0.685, 0.685, 0.685, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "gradient",
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"gradient" : 0,
					"id" : "obj-213",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 14.0, 1061.0, 51.0, 22.0 ],
					"style" : "",
					"text" : "/refresh",
					"textcolor" : [ 0.0, 0.0, 0.0, 1.0 ]
				}
*/

func (a *monwig) ActionPressed(btn dawcontrol.ActionButton) {
	switch btn {
	case dawcontrol.TRIGGER_BUTTON:
		a.Lock()
		a.showFilledMode = true
		a.Unlock()
	case dawcontrol.PLAY_BUTTON:
		bitwig.Play.WriteTo(a.layout)
		return
	case dawcontrol.UNDO_BUTTON:
		bitwig.Undo.WriteTo(a.layout)
	case dawcontrol.DELETE_BUTTON:
		a.Lock()
		a.showFilledMode = true
		a.fillOrDeleteMode = true
		a.Unlock()
		bitwig.Refresh.WriteTo(a.layout)
		//a.Mapper.printFilled()
	case dawcontrol.AUTOMATION_BUTTON:
		bitwig.AutowriteToggle.WriteTo(a.layout)
		return
	case dawcontrol.OVERDUB_BUTTON:
		bitwig.OverdubLauncher.WriteTo(a.layout)
	case dawcontrol.METRONOME_BUTTON:
		bitwig.Click.WriteTo(a.layout)
		return
	case dawcontrol.TEMPO_TAP_BUTTON:
		bitwig.TempoTap.WriteTo(a.layout)
	}

	a.layout.SwitchActionButton(btn, true)
}

func (a *monwig) TrackStop(track uint8) {
	//	bitwig.TrackRecArmToggle.Set(track + 1).WriteTo(a.layout)
	bitwig.TrackClipStop.Set(track + 1).WriteTo(a.layout)
	a.layout.SetActiveTrackClip(track, 0)
}

func (a *monwig) ScenePressed(scene uint8) bool {
	//	fmt.Printf("trigger scene %d\n", scene)
	var foundFilled bool
	for track := uint8(0); track < a.layout.NumTracks(); track++ {
		if a.layout.IsFilledClip(track, scene) {
			foundFilled = true
		}
	}

	if foundFilled {
		a.layout.BlankActiveTrackClips()
	}

	for track := uint8(0); track < a.layout.NumTracks(); track++ {
		if a.layout.IsFilledClip(track, scene) {
			a.layout.SetActiveTrackClip(track, a.sceneNumber(scene))
		} else {
			a.layout.SetActiveTrackClip(track, 0)
		}
	}
	if scene > 7 {
		//		/scene/bank/{+,-}
		bitwig.SceneBankNext.WriteTo(a.layout)
	} else {
		bitwig.SceneBankPrevious.WriteTo(a.layout)
	}
	time.Sleep(time.Millisecond * 5)
	bitwig.SceneLaunch.Set(a.sceneNumber(scene)).WriteTo(a.layout)

	return true
}

// sceneNumber returns the scene number for a scene (counting from 0) for bitwig (counting from 1)
func (a *monwig) sceneNumber(scene uint8) uint8 {
	return (scene % 8) + 1
}

// clipNumber returns the clip number for a clip (counting from 0) for bitwig (counting from 1)
func (a *monwig) clipNumber(clip uint8) uint8 {
	return (clip % 8) + 1
}

func (a *monwig) ClipPressed(track, clip uint8) {
	a.RLock()
	if a.fillOrDeleteMode {
		a.RUnlock()
		a.doFillOrDelete(track, clip)
		return
	}
	a.RUnlock()

	a.Lock()
	a.selectedClip[0] = track + 1
	a.selectedClip[1] = clip + 1
	a.Unlock()

	for tr := uint8(0); tr < a.layout.NumTracks(); tr++ {
		//fmt.Printf("set track arming to %v for track %v\n", tr == track, tr)
		//fmt.Println(string(bitwig.TrackRecArm.Set(tr + 1)))
		bitwig.TrackRecArm.Set(tr+1).WriteTo(a.layout, tr == track)
	}
}

func (a *monwig) StopAllClips() {
	a.layout.BlankActiveTrackClips()

	for i := uint8(0); i < a.layout.NumTracks(); i++ {
		bitwig.TrackClipStop.Set(i + 1).WriteTo(a.layout)
	}
}

func (a *monwig) eachTrackMessage(path osc.Path, vals ...interface{}) {
	if path.HasPrefix("/track/selected") {
		return
	}
	switch {

	//bitwig said: /track/1/clip/2/isSelected [0]
	case path.HasSuffix("isSelected"):
		var track, clip uint8
		_, err := path.Scan("/track/%d/clip/%d/isSelected", &track, &clip)
		if err != nil {
			fmt.Printf("can't scan %q: %v\n", path.String(), err)
			return
		}

		if track > a.layout.NumTracks() || clip > a.layout.NumClipsPerTrack() {
			return
		}

		//		fmt.Printf("has content: track: %v clip: %v: %v\n", track, clip, bitwig.HasAndIsOn(vals))
		//		a.Lock()
		//if bitwig.HasAndIsOn(vals) {
		//			a.layout.FillClip(track-1, clip-1)
		//} else {
		//			a.layout.UnfillClip(track-1, clip-1)
		//}

		// bitwig said: /track/1/clip/2/isPlaying [0]
	case path.HasSuffix("isPlaying"):
		var track, clip uint8
		_, err := path.Scan("/track/%d/clip/%d/isPlaying", &track, &clip)
		if err != nil {
			fmt.Printf("can't scan %q: %v\n", path.String(), err)
			return
		}

		if track > a.layout.NumTracks() || clip > a.layout.NumClipsPerTrack() {
			return
		}

		//		fmt.Printf("has content: track: %v clip: %v: %v\n", track, clip, bitwig.HasAndIsOn(vals))
		//		a.Lock()
		if bitwig.HasAndIsOn(vals) {
			a.layout.FillClip(track-1, clip-1)
			a.layout.SetActiveTrackClip(track-1, clip)
		}

		// bitwig said: /track/1/clip/2/isRecording [0]
	case path.HasSuffix("isRecording"):
		var track, clip uint8
		_, err := path.Scan("/track/%d/clip/%d/isRecording", &track, &clip)
		if err != nil {
			fmt.Printf("can't scan %q: %v\n", path.String(), err)
			return
		}

		if track > a.layout.NumTracks() || clip > a.layout.NumClipsPerTrack() {
			return
		}

		//		fmt.Printf("has content: track: %v clip: %v: %v\n", track, clip, bitwig.HasAndIsOn(vals))
		//		a.Lock()
		if bitwig.HasAndIsOn(vals) {
			a.layout.FillClip(track-1, clip-1)
			a.layout.SetActiveTrackClip(track-1, clip)
		}

		//bitwig said: /track/1/clip/2/isPlayingQueued [0]
		//bitwig said: /track/1/clip/2/isRecordingQueued [0]
		//bitwig said: /track/1/clip/2/isStopQueued [0]

	// /track/8/clip/1/hasContent
	case path.HasSuffix("hasContent"):
		var track, clip uint8
		_, err := path.Scan("/track/%d/clip/%d/hasContent", &track, &clip)
		if err != nil {
			fmt.Printf("can't scan %q: %v\n", path.String(), err)
			return
		}

		if track > a.layout.NumTracks() || clip > a.layout.NumClipsPerTrack() {
			return
		}

		//		fmt.Printf("has content: track: %v clip: %v: %v\n", track, clip, bitwig.HasAndIsOn(vals))
		//		a.Lock()
		if bitwig.HasAndIsOn(vals) {
			a.layout.FillClip(track-1, clip-1)
		} else {
			a.layout.UnfillClip(track-1, clip-1)
		}
		//		a.Unlock()
	}
	/*
		idx := strings.LastIndex(path.String(), "/")
		if idx < 0 {
			panic("must not happen")
		}
		suffix := path.String()[idx:]

		switch suffix {
		case "/recarm":
			var track int
			path.Scan(string(bitwig.TrackRecArm), &track)
			a.layout.SwitchTrack(uint8(track-1), bitwig.HasAndIsOn(vals))
		}
	*/
}

func (a *monwig) EachMessage(path osc.Path, vals ...interface{}) {
	//	fmt.Printf("bitwig said: %s %v\n", path, vals)

	switch path.String() {
	case string(bitwig.Autowrite):
		a.layout.SwitchActionButton(dawcontrol.AUTOMATION_BUTTON, bitwig.HasAndIsOn(vals))
	case string(bitwig.Click):
		a.layout.SwitchActionButton(dawcontrol.METRONOME_BUTTON, bitwig.HasAndIsOn(vals))
	case string(bitwig.OverdubLauncher):
		a.layout.SwitchActionButton(dawcontrol.OVERDUB_BUTTON, bitwig.HasAndIsOn(vals))
	case string(bitwig.Play):
		a.Lock()
		a.playing = bitwig.HasAndIsOn(vals)
		a.Unlock()
	default:
		if path.HasPrefix("/track") {
			a.eachTrackMessage(path, vals...)
		}
	}

	//switch path.String() {
	/*
		case "/fxparam/last_touched/name":
			if len(vals) > 0 {
				if s, is := vals[0].(string); is {
					fmt.Printf("got %#v\n", s)
					//				Slot 3, 2 Record
					if strings.HasSuffix(s, "Record") && strings.HasPrefix(s, "Slot") {
						sgs := strings.Split(s, " ")
						clip, _ := strconv.Atoi(sgs[1][0:1])
						track, _ := strconv.Atoi(sgs[2])
						fmt.Printf("got track %d clip %d\n", track, clip)
						m.Lock()
						m.trackClip[track-1] = setTrackClipRecNo(uint8(clip - 1))
						m.Unlock()
						return
					}

					//				Record selected slot
					if strings.HasPrefix(s, "Record selected slot") {
						m.Lock()
						m.trackClip[m.lastRecorded[0]] = setTrackClipRecNo(m.lastRecorded[1])
						m.Unlock()
						return
					}
				}
			}
	*/
	//		[Record selected slot, go down (advanced)]
	/*
		case string(reaper.Autowrite):
			m.switchLightByVal(vals, 7, 4)
	*/

	//case string(reaper.METRONOME):
	//	a.Mapper.SwitchActionButton(METRONOME_BUTTON, reaper.HasAndIsOn(vals))
	//case string(reaper.STOP):
	//	playing := !reaper.HasAndIsOn(vals)
	//	a.Lock()
	//	a.playing = playing
	//	a.Unlock()
	//	a.Mapper.SwitchActionButton(PLAY_BUTTON, playing)
	/*
		case string(reaper.PLAY):
			m.Lock()
			m.playing = reaper.HasAndIsOn(vals)
			m.Unlock()
			m.switchLightByVal(vals, 7, 1)
	*/
	/*
		default:
			if path.HasPrefix("/track") {
				a.eachTrackMessage(path, vals...)
			}
		}
	*/
}
